__author__ = 'Vladimir Volodavets'

import argparse
import re

EMAIL_PATTERN = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
DATE_PATTERN = r"^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$"


class ValidateIntAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        try:
            int(values)
            setattr(namespace, self.dest, values)
        except ValueError:
            print "argument of --validint is not int"


class ValidateFloatAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        try:
            float(values)
            setattr(namespace, self.dest, values)
        except ValueError:
            print "argument of --validfloat is not float"


class ValidateEmailAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):
        email_regex = re.compile(EMAIL_PATTERN)

        if email_regex.match(values):
            setattr(namespace, self.dest, values)
        else:
            print "argument of --validemail is not email address"


class ValidateISODateAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        date_regex = re.compile(DATE_PATTERN)
        if date_regex.match(values):
            setattr(namespace, self.dest, values)
        else:
            print "argument of --validisodate doesn't match YYYY-MM-DD"


class ValidateCsAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if ',' in values:
            setattr(namespace, self.dest, values)
        else:
            print "argument of --validcs has no comma "


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--validint', nargs='?', action=ValidateIntAction)
    parser.add_argument('--validfloat', action=ValidateFloatAction)
    parser.add_argument('--validemail', action=ValidateEmailAction)
    parser.add_argument('--validisodate', action=ValidateISODateAction)
    parser.add_argument('--validcs', action=ValidateCsAction)

    args = parser.parse_args()

    print args
